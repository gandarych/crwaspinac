#!/bin/bash

###### 05_01_install_ansible on ubuntu WHEN WE WORK AS ROOT

# docker run -it ubuntu bash

# Your command prompt should change to reflect the fact that you're now working inside the container:

## OUTPUT:
#kris@gandalf1:~$     docker run -it ubuntu bash
#root@dfffe6a3e74d:/#

# Pay attention! Note the container ID in the command prompt: it is dfffe6a3e74d.
# You'll need that container ID later to identify the container when you want to remove it.


# Now you can run any command inside the container:

apt update

# Installing any application in it. Let's install the Node.js:

apt install -y nodejs

# It installs Node.js in the container from the official Ubuntu repository.
# "Do you want to continue? [Y/n]"
### >> type "y" + ENTER
# Verify that Node.js is installed to see the version number:

node -v

# Any changes you make inside the container only apply to that container.
# To exit the container, type "exit" at the prompt.

apt install -y ansible
## apt install pip
## pip install ansible

ansible --version

## OUTPUT:
####  root@dfffe6a3e74d:/# ansible --version
####  ansible 2.5.1
####    config file = /etc/ansible/ansible.cfg
####    configured module search path = [u'/root/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
####    ansible python module location = /usr/lib/python2.7/dist-packages/ansible
####    executable location = /usr/bin/ansible
####    python version = 2.7.17 (default, Nov  7 2019, 10:07:09) [GCC 7.4.0]
####  root@dfffe6a3e74d:/# 

apt install -y net-tools
ifconfig

###  write "hosts" inventory file:
#[all:vars]
#ansible_user=dev
#[server22]
#192.168.1.22

cd /home/
mkdir kris
cd /home/kris/
touch hosts
echo "[all:vars]" > hosts
echo "ansible_user=dev" >> hosts 
echo "[server22]" >> hosts 
echo "192.168.1.22" >> hosts 
cat hosts

###  Let's create sample nginx.yml:
#
#- hosts: all
#  become: yes
#  tasks:
#    - name: Install nginx
#      apt: pkg=nginx state=present update_cache=true

cd /home/kris/
touch nginx.yml
echo "" > nginx.yml
echo "- hosts: all" >> nginx.yml
echo "  become: yes" >> nginx.yml
echo "  tasks:" >> nginx.yml
echo "    - name: Install nginx" >> nginx.yml
echo "      apt: pkg=nginx state=present update_cache=true" >> nginx.yml
cat nginx.yml

###  and then:

ansible -m ping -i hosts all

## OUTPUT:
####   192.168.1.22 | UNREACHABLE! => {
####       "changed": false, 
####       "msg": "[Errno None] Unable to connect to port 22 on 192.168.1.22", 
####       "unreachable": true
####   }

apt install -y ping
##or## apt-get install -y ping

apt install -y iputils-ping
##or## apt install -y inetutils-ping

apt install -y telnet
##or## apt-get install -y telnet

ansible --help

ansible -m ping -i hosts all

###  And run the command now:

ansible-playbook --help

ansible-playbook --inventory-file=/home/kris/hosts nginx.yml
# ansible-playbook -i hosts -l nginx.yml

ping 192.168.1.22
## telnet 192.168.1.22 22
## telnet 192.168.1.22 13531 


####   https://github.com/startup-systems/terraform-ansible-example
####   https://www.hashicorp.com/resources/ansible-terraform-better-together
####   https://alex.dzyoba.com/blog/terraform-ansible/
####   https://medium.com/faun/building-repeatable-infrastructure-with-terraform-and-ansible-on-aws-3f082cd398ad
####   https://victorops.com/blog/writing-ansible-playbooks-for-new-terraform-servers
####   https://getintodevops.com/blog/using-ansible-with-terraform
####   https://www.redhat.com/cms/managed-files/pa-terraform-and-ansible-overview-f14774wg-201811-en.pdf
####   https://stackshare.io/stackups/ansible-vs-terraform    (LOGO, following)

####   Packer provisioner runs Ansible playbooks!!!:
####   https://packer.io/docs/provisioners/ansible.html

